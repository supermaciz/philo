/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_render.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/21 19:10:36 by plastic           #+#    #+#             */
/*   Updated: 2015/04/01 13:16:40 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philo.h>
#include <sdl_pack.h>
#include <SDL_mixer.h>
#include <pthread.h>
#include <libft.h>
#include <sdl_assets.h>

void			load_background(t_data *data, char *sprite, short update)
{
	SDL_Rect	dest;
	SDL_Surface	*back;

	dest.x = 0;
	dest.y = 0;
	dest.w = 0;
	dest.h = 0;
	back = NULL;
	back = SDL_LoadBMP(sprite);
	if (!back)
	{
		ft_putendl("Loading sprite: fatal error");
		ft_exit (data, 1, NULL);
	}
	SDL_BlitSurface(back, NULL, SDL_GetWindowSurface(data->win), &dest);
	SDL_FreeSurface(back);
	if (update)
		SDL_UpdateWindowSurface(data->win);
}

static void		draw_lifebar(t_data *d, size_t hp, size_t pos, SDL_Window *win)
{
	SDL_Rect	dest;
	SDL_Surface	*play;

	dest.x = 109;
	dest.y = pos;
	dest.w = 0;
	dest.h = 0;
	hp = (hp * WIDTH_LB) / MAX_LIFE;
	play = NULL;
	play = SDL_CreateRGBSurface(SDL_SWSURFACE, hp, HEIGHT_LB, 32, 0, 0, 0, 0);
	if (!play)
	{
		ft_putendl("Loading lifebar sprite: fatal error");
		ft_exit (d, 1, NULL);
	}
	SDL_FillRect(play, NULL, SDL_MapRGB(play->format, 252, 232, 0));
	SDL_BlitSurface(play, NULL, SDL_GetWindowSurface(win), &dest);
	SDL_FreeSurface(play);
}

static void		update_sprite(t_data *d, t_action stat, int philo)
{
	SDL_Surface	*play;
	SDL_Rect	dest;
	char		*str;

	play = NULL;
	dest.x = g_assets[philo].pos_x;
	dest.y = g_assets[philo].pos_y;
	if (stat == EAT)
	{
		str = ft_strjoin("./assets/sprites/", g_assets[philo].eat_sp);
		play = SDL_LoadBMP(str);
		free(str);
	}
	else if (stat == THINK)
	{
		str = ft_strjoin("./assets/sprites/", g_assets[philo].think_sp);
		play = SDL_LoadBMP(str);
		free(str);
	}
	if (play)
	{
		SDL_BlitSurface(play, NULL, SDL_GetWindowSurface(d->win), &dest);
		SDL_FreeSurface(play);
	}
}

int				update_philo(t_data *data, int philo)
{
	int			life;
	t_action	stat;
	Mix_Chunk	*sound;
	char		*str;

	pthread_mutex_lock(&data->philos[philo]->live.mutex);
	life = data->philos[philo]->live.life_nb;
	pthread_mutex_unlock(&data->philos[philo]->live.mutex);
	pthread_mutex_lock(&data->philos[philo]->mutex);
	stat = data->philos[philo]->action;
	pthread_mutex_unlock(&data->philos[philo]->mutex);
	draw_lifebar(data, life, g_assets[philo].life_y, data->win);
	update_sprite(data, stat, philo);
	if (stat == EAT && !Mix_Playing(philo + 1))
	{
		sound = NULL;
		str = ft_strjoin("./assets/sounds/", g_assets[philo].sound);
		play_sound("./assets/sounds/fight.wav", philo + 1, 0, &sound);
		free(str);
		Mix_FreeChunk(sound);
	}
	if (!life)
		return (1);
	return (0);
}

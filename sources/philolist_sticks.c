/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philolist_sticks.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/26 22:40:01 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/26 22:40:04 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <philo.h>

t_clist			*philo_list(t_clist *philosophers)
{
	static t_clist			*list = NULL;

	if (philosophers == NULL)
		return (list);
	else
		list = philosophers;
	return (list);
}

t_chopstick		*stick_array(t_chopstick *stick)
{
	static t_chopstick		*array = NULL;

	if (stick == NULL)
		return (array);
	else
		array = stick;
	return (array);
}

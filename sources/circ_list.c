/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   circ_list.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/07 00:04:34 by mcizo             #+#    #+#             */
/*   Updated: 2015/03/10 17:29:35 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <philo.h>

t_clist	*clst_new(void)
{
	t_elem		*sentinel;
	t_clist		*new_list;

	sentinel = (t_elem *)malloc(sizeof(*sentinel));
	new_list = (t_clist *)malloc(sizeof(*new_list));
	if (sentinel != NULL && new_list != NULL)
	{
		sentinel->id = -42;
		sentinel->next = sentinel;
		sentinel->prev = sentinel;
		new_list->sentinel = sentinel;
		new_list->len = 0;
	}
	return (new_list);
}

void	clstadd_end(t_clist *lst)
{
	t_elem		*new_elem;

	new_elem = (t_elem *)malloc(sizeof(*new_elem));
	if (new_elem != NULL)
	{
		new_elem->next = lst->sentinel;
		new_elem->prev = lst->sentinel->prev;
		lst->sentinel->prev->next = new_elem;
		lst->sentinel->prev = new_elem;
		lst->len += 1;
	}
}

void	clstadd_begin(t_clist *lst)
{
	t_elem	*new_elem;

	new_elem = (t_elem *)malloc(sizeof(*new_elem));
	if (new_elem != NULL)
	{
		new_elem->next = lst->sentinel->next;
		new_elem->prev = lst->sentinel;
		lst->sentinel->next->prev = new_elem;
		lst->sentinel->next = new_elem;
		lst->len += 1;
	}
}

void	clst_del_elem(t_elem *element, t_clist *lst)
{
	if (element->next != element && element != NULL)
	{
		element->prev->next = element->next;
		element->next->prev = element->prev;
		free(element);
		pthread_mutex_destroy(&element->mutex);
		pthread_mutex_destroy(&element->hands.mutex);
		pthread_mutex_destroy(&element->live.mutex);
		lst->len -= 1;
	}
}

void	clst_delete(t_clist **lst)
{
	t_elem	*current;
	t_elem	*next_elem;

	if (lst == NULL)
		return ;
	current = (*lst)->sentinel->next;
	while (current != (*lst)->sentinel && current != NULL)
	{
		next_elem = current->next;
		free(current);
		current = next_elem;
	}
	free(current);
	free(*lst);
	*lst = NULL;
}

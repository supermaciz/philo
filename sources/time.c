/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/08 22:15:42 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/02 20:36:52 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <errno.h>
#include <libft.h>
#include <philo.h>

char	*end_game(char *death)
{
	static char		*end = NULL;

	if (death != NULL)
		end = death;
	return (end);
}

char	*start_game(char *go)
{
	static char		*start = NULL;

	if (go != NULL)
		start = go;
	return (start);
}

void	*dinner_timer(void *arg)
{
	t_data		*data;

	data = (t_data *)arg;
	usleep(1010000);
	while (data->timer > 0)
	{
		pthread_mutex_lock(&data->timer_mutex);
		data->timer -= 1;
		pthread_mutex_unlock(&data->timer_mutex);
		usleep(1000000);
	}
	return (NULL);
}

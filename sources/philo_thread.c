/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_thread.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/10 22:37:34 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/04 21:23:49 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <philo.h>
#include <stdlib.h>
#include <unistd.h>
#include <philo_global_var.h>

static void		philo_start(t_elem *philo, t_chopstick *stick)
{
	philo->live.last_meal = time(NULL);
	stick = stick_array(NULL);
	get_free_stick(philo, stick, 1);
	pthread_mutex_lock(&philo->hands.mutex);
	set_action(philo);
	pthread_mutex_unlock(&philo->hands.mutex);
}

void			*philo_actions(void *arg)
{
	t_elem			*philo;
	t_chopstick		*stick;

	philo = arg;
	usleep(1000000);
	stick = stick_array(NULL);
	philo_start(philo, stick);
	while (is_alive(philo) == TRUE)
	{
		which_action(philo, stick);
		pthread_mutex_lock(&g_data->timer_mutex);
		if (g_data->death == TRUE || g_data->timer == 0)
		{
			pthread_mutex_unlock(&g_data->timer_mutex);
			return (NULL);
		}
		pthread_mutex_unlock(&g_data->timer_mutex);
	}
	pthread_mutex_lock(&g_data->timer_mutex);
	g_data->death = TRUE;
	pthread_mutex_unlock(&g_data->timer_mutex);
	return (NULL);
}

t_bool			is_alive(t_elem *philo)
{
	pthread_mutex_lock(&philo->live.mutex);
	if (philo->action != EAT)
	{
		philo->live.life_nb -= time(NULL) - philo->live.last_meal;
		philo->live.last_meal = time(NULL);
		if (philo->live.life_nb < 0)
			philo->live.life_nb = 0;
		if (philo->live.life_nb > 0)
		{
			pthread_mutex_unlock(&philo->live.mutex);
			return (TRUE);
		}
	}
	else
	{
		philo->live.life_nb = MAX_LIFE;
		pthread_mutex_unlock(&philo->live.mutex);
		return (TRUE);
	}
	pthread_mutex_unlock(&philo->live.mutex);
	return (FALSE);
}

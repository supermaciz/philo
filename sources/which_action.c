/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   which_action.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 14:47:59 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/22 21:16:03 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <philo.h>
#include <philo_global_var.h>

void			set_action(t_elem *philo)
{
	if (philo->hands.stick_nb == 0)
		philo->action = REST;
	else if (philo->hands.stick_nb == 1)
		philo->action = THINK;
	else if (philo->hands.stick_nb == 2)
	{
		philo->action = EAT;
	}
}

static void		wait_eat_time(useconds_t waiting)
{
	usleep(waiting);
}

static void		wait_think_time(useconds_t waiting, t_elem *philo)
{
	while (waiting > 0)
	{
		usleep(200000);
		if (is_alive(philo) == FALSE)
		{
			pthread_mutex_lock(&g_data->timer_mutex);
			g_data->death = TRUE;
			pthread_mutex_unlock(&g_data->timer_mutex);
			break ;
		}
		waiting -= 200000;
		pthread_mutex_lock(&philo->hands.mutex);
		set_action(philo);
		pthread_mutex_unlock(&philo->hands.mutex);
		if (philo->action == REST)
			break ;
	}
}

static void		wait_time(useconds_t waiting, t_elem *philo)
{
	while (waiting > 0)
	{
		usleep(500000);
		if (is_alive(philo) == FALSE)
		{
			pthread_mutex_lock(&g_data->timer_mutex);
			g_data->death = TRUE;
			pthread_mutex_unlock(&g_data->timer_mutex);
			break ;
		}
		waiting -= 500000;
	}
}

void			which_action(t_elem *philo, t_chopstick *stick)
{
	useconds_t	waiting;

	if (philo->action == REST)
	{
		waiting = REST_T * 1000000;
		wait_time(waiting, philo);
		rest(philo, stick);
	}
	else if (philo->action == THINK)
	{
		waiting = THINK_T * 1000000;
		wait_think_time(waiting, philo);
		think(philo, stick);
	}
	else if (philo->action == EAT)
	{
		waiting = EAT_T * 1000000;
		wait_eat_time(waiting);
		eat(philo, stick);
	}
}

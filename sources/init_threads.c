/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_threads.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/10 22:19:15 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/23 22:44:27 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <philo.h>
#include <sdl_pack.h>
#include <libft.h>
#include <philo_global_var.h>

t_data		*g_data;

static void		start_meal(t_clist *philosophers, t_chopstick *stick,
							t_data *data)
{
	t_elem			*current;

	philo_list(philosophers);
	create_sticks(philosophers, stick);
	stick_array(stick);
	current = philosophers->sentinel->next;
	data->win = sdl_init();
	data->stick = stick;
	start_game("ok");
	while (current != philosophers->sentinel)
	{
		pthread_detach(current->thread);
		current = current->next;
	}
	sdl_loop(data);
}

static int		conf_thread(t_clist *philosophers)
{
	LASTPHILO->hands.stick_nb = 0;
	LASTPHILO->live.last_meal = time(NULL);
	LASTPHILO->live.life_nb = MAX_LIFE;
	if (pthread_create(&(LASTPHILO->thread), NULL, philo_actions,
				LASTPHILO) != 0)
	{
		ft_putendl("pthread_create error");
		return (EXIT_FAILURE);
	}
	if ((pthread_mutex_init(&LASTPHILO->mutex, NULL)) != 0)
	{
		ft_printf("pthread_mutex_init error: %d\n");
		return (EXIT_FAILURE);
	}
	if ((pthread_mutex_init(&LASTPHILO->hands.mutex, NULL)) != 0)
	{
		ft_printf("pthread_mutex_init error: %d\n");
		return (EXIT_FAILURE);
	}
	if ((pthread_mutex_init(&LASTPHILO->live.mutex, NULL)) != 0)
	{
		ft_printf("pthread_mutex_init 2 error: %d\n");
		return (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);
}

static void		philo_init(t_clist *philosophers, int i)
{
	LASTPHILO->id = i;
	LASTPHILO->stick1 = i;
	LASTPHILO->stick2 = (i < 6) ? i + 1 : 0;
}

void			init_threads(void)
{
	t_clist			*philosophers;
	t_chopstick		stick[7];
	int				i;
	pthread_t		time_thread;
	t_data			data;

	i = 0;
	conf_data(&data);
	g_data = &data;
	if (pthread_create(&time_thread, NULL, dinner_timer, &data) != 0)
	{
		ft_putendl("pthread_create error, timer");
		return ;
	}
	pthread_detach(time_thread);
	philosophers = clst_new();
	while (i < 7)
	{
		clstadd_end(philosophers);
		philo_init(philosophers, i);
		conf_thread(philosophers);
		data.philos[i] = LASTPHILO;
		i++;
	}
	start_meal(philosophers, stick, &data);
}

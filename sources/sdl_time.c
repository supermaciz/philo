/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_time.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/25 15:11:11 by plastic           #+#    #+#             */
/*   Updated: 2015/04/01 13:14:55 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philo.h>
#include <SDL.h>
#include <sdl_pack.h>
#include <sdl_sprites.h>

static void		display_digit(SDL_Window *win, SDL_Rect dest, short digit)
{
	SDL_Surface	*play;
	Uint32		colorkey;

	play = NULL;
	if (dest.x >= 0)
	{
		play = SDL_LoadBMP(g_digit_sprites[digit]);
		if (play)
		{
			colorkey = SDL_MapRGB(play->format, 255, 0, 255);
			SDL_SetColorKey(play, SDL_TRUE, colorkey);
			SDL_BlitSurface(play, NULL, SDL_GetWindowSurface(win), &dest);
			SDL_FreeSurface(play);
		}
	}
}

void			display_time(SDL_Window *win, int time)
{
	short		digit;
	short		count;
	short		first;
	SDL_Rect	dest;

	count = 1;
	first = 1;
	while (time || first)
	{
		digit = time % 10;
		time /= 10;
		dest.x = 1280 - MARGIN - (DIGIT_WIDTH * count);
		dest.y = DIGIT_Y;
		display_digit(win, dest, digit);
		count++;
		first = 0;
	}
}

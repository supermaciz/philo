/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putdown_sticks.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 15:24:10 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/24 17:21:04 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <stdlib.h>
#include <philo.h>
#include <libft.h>

void			putdown_sticks(t_elem *philo, t_chopstick *stick)
{
	int		err;

	pthread_mutex_lock(&philo->hands.mutex);
	if ((err = pthread_mutex_trylock(&stick[philo->stick1].mutex)) != EBUSY
		&& stick[philo->stick1].owner == philo)
	{
		stick[philo->stick1].owner = NULL;
		philo->hands.stick_nb -= 1;
	}
	if (err == 0)
		pthread_mutex_unlock(&stick[philo->stick1].mutex);
	if ((err = pthread_mutex_trylock(&stick[philo->stick2].mutex)) != EBUSY
		&& stick[philo->stick2].owner == philo)
	{
		stick[philo->stick2].owner = NULL;
		philo->hands.stick_nb -= 1;
	}
	if (err == 0)
		pthread_mutex_unlock(&stick[philo->stick2].mutex);
	pthread_mutex_unlock(&philo->hands.mutex);
}

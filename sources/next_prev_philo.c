/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   next_prev_philo.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/22 21:43:06 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/22 22:15:55 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philo.h>

t_elem		*get_next_philo(t_elem *philo)
{
	t_clist		*list;

	list = philo_list(NULL);
	if (philo->next == list->sentinel)
		return (philo->next->next);
	return (philo->next);
}

t_elem		*get_prev_philo(t_elem *philo)
{
	t_clist		*list;

	list = philo_list(NULL);
	if (philo->prev == list->sentinel)
		return (philo->prev->prev);
	return (philo->prev);
}

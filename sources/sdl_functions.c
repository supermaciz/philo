/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_functions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 20:18:32 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/01 13:14:43 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <SDL.h>
#include <SDL_mixer.h>
#include <libft.h>
#include <stdlib.h>
#include <philo.h>
#include <sdl_pack.h>

SDL_Window	*sdl_init(void)
{
	SDL_Window	*win;

	win = NULL;
	if (SDL_Init(SDL_INIT_VIDEO) != 0 ||
		Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024)
		== -1)
		ft_putendl("SDL init: fatal error");
	else
	{
		win = SDL_CreateWindow("Super Philo Fighter II",
									SDL_WINDOWPOS_UNDEFINED,
									SDL_WINDOWPOS_UNDEFINED,
									1280, 720, SDL_WINDOW_SHOWN);
		if (!win)
			ft_putendl("Windows init: fatal error");
	}
	Mix_AllocateChannels(16);
	return (win);
}

static int	do_one_cycle(t_data *data)
{
	short		end;
	short		ret;
	short		philo;
	int			time;

	end = 0;
	ret = 0;
	philo = 0;
	pthread_mutex_lock(&data->timer_mutex);
	if ((time = data->timer) == 0)
		end = 2;
	pthread_mutex_unlock(&data->timer_mutex);
	load_background(data, "./assets/sprites/back.bmp", 0);
	display_time(data->win, time);
	while (philo < 7)
		ret += update_philo(data, philo++);
	if (ret)
		end = 1;
	SDL_UpdateWindowSurface(data->win);
	return (end);
}

static void	display_end_title(SDL_Window *win)
{
	SDL_Rect	dest;
	SDL_Surface	*play;
	Uint32		colorkey;

	play = NULL;
	dest.x = 400;
	dest.y = 100;
	play = SDL_LoadBMP("./assets/sprites/dance.bmp");
	if (play)
	{
		colorkey = SDL_MapRGB(play->format, 255, 0, 255);
		SDL_SetColorKey(play, SDL_TRUE, colorkey);
		SDL_BlitSurface(play, NULL, SDL_GetWindowSurface(win), &dest);
		SDL_FreeSurface(play);
		SDL_UpdateWindowSurface(win);
	}
}

static void	end_loop(t_data *data, int ret, Mix_Chunk *sound)
{
	SDL_Event	ev;

	if (ret == 2)
	{
		play_sound("./assets/sounds/youwin.wav", 0, 0, &sound);
		ft_putendl("Now, it is time... To DAAAAAAAANCE!!!");
		display_end_title(data->win);
	}
	else
		play_sound("./assets/sounds/youlose.wav", 0, 0, &sound);
	while (42)
	{
		SDL_WaitEvent(&ev);
		if ((ev.type == SDL_KEYUP && ev.key.keysym.sym == SDLK_ESCAPE)
			|| ev.type == SDL_QUIT)
			ft_exit (data, 0, sound);
	}
}

void		sdl_loop(t_data *data)
{
	SDL_Event	ev;
	int			ret;
	Mix_Chunk	*sound;

	sound = NULL;
	load_background(data, "./assets/sprites/capcom.bmp", 1);
	play_sound("./assets/sounds/fight.wav", 0, 0, &sound);
	SDL_Delay(800);
	play_sound("./assets/sounds/music.wav", 0, -1, &sound);
	while ((ret = do_one_cycle(data)) == 0)
	{
		if (SDL_PollEvent(&ev))
			if ((ev.type == SDL_KEYUP && ev.key.keysym.sym == SDLK_ESCAPE)
				|| ev.type == SDL_QUIT)
				ft_exit (data, 0, sound);
	}
	end_loop(data, ret, sound);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   post_think.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/27 00:14:29 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/27 18:09:06 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <philo.h>
#include <philo_global_var.h>

static void		find_stick(t_elem *philo, t_chopstick *stick)
{
	get_free_stick(philo, stick, 2);
	pthread_mutex_lock(&philo->hands.mutex);
	if (philo->hands.stick_nb < 2)
	{
		pthread_mutex_unlock(&philo->hands.mutex);
		get_thinker_stick(philo, stick, 2 - philo->hands.stick_nb);
		pthread_mutex_lock(&philo->hands.mutex);
	}
	set_action(philo);
	pthread_mutex_unlock(&philo->hands.mutex);
}

void			post_think_state(t_elem *philo, t_chopstick *stick)
{
	while (philo->action == THINK)
	{
		pthread_mutex_lock(&philo->hands.mutex);
		set_action(philo);
		pthread_mutex_unlock(&philo->hands.mutex);
		if (philo->action == REST)
			return ;
		usleep(40000);
		if (is_alive(philo) == FALSE)
		{
			pthread_mutex_lock(&g_data->timer_mutex);
			g_data->death = TRUE;
			pthread_mutex_unlock(&g_data->timer_mutex);
			return ;
		}
		find_stick(philo, stick);
	}
}

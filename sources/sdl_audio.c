/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_audio.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/22 01:19:28 by plastic           #+#    #+#             */
/*   Updated: 2015/04/01 13:11:28 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <SDL.h>
#include <SDL_mixer.h>
#include <libft.h>

void play_sound(char *path, short channel, short n_time, Mix_Chunk **sound)
{
	if (*sound != NULL)
		Mix_FreeChunk(*sound);
	*sound = Mix_LoadWAV(path);
	if (*sound)
	{
		Mix_VolumeChunk(*sound, MIX_MAX_VOLUME);
		Mix_PlayChannel(channel, *sound, n_time);
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_actions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/08 14:59:45 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/27 18:09:05 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <libft.h>
#include <philo.h>
#include <philo_global_var.h>

/*
** unistd.h: usleep()
*/

static void	temporary_state(t_elem *philo, t_chopstick *stick)
{
	while (philo->action == REST)
	{
		usleep(60000);
		if (is_alive(philo) == FALSE)
		{
			pthread_mutex_lock(&g_data->timer_mutex);
			g_data->death = TRUE;
			pthread_mutex_unlock(&g_data->timer_mutex);
			return ;
		}
		get_free_stick(philo, stick, 2);
		pthread_mutex_lock(&philo->hands.mutex);
		if (philo->hands.stick_nb < 2)
		{
			pthread_mutex_unlock(&philo->hands.mutex);
			get_thinker_stick(philo, stick, 2 - philo->hands.stick_nb);
			pthread_mutex_lock(&philo->hands.mutex);
		}
		set_action(philo);
		pthread_mutex_unlock(&philo->hands.mutex);
	}
}

void		eat(t_elem *philo, t_chopstick *stick)
{
	pthread_mutex_lock(&philo->live.mutex);
	philo->live.life_nb = MAX_LIFE;
	philo->live.last_meal = time(NULL);
	pthread_mutex_unlock(&philo->live.mutex);
	pthread_mutex_lock(&philo->mutex);
	philo->action = REST;
	pthread_mutex_unlock(&philo->mutex);
	putdown_sticks(philo, stick);
}

void		rest(t_elem *philo, t_chopstick *stick)
{
	get_free_stick(philo, stick, 2);
	pthread_mutex_lock(&philo->hands.mutex);
	if (philo->hands.stick_nb < 2)
	{
		pthread_mutex_unlock(&philo->hands.mutex);
		get_thinker_stick(philo, stick, 2 - philo->hands.stick_nb);
		pthread_mutex_lock(&philo->hands.mutex);
		set_action(philo);
		pthread_mutex_unlock(&philo->hands.mutex);
		return ;
	}
	set_action(philo);
	pthread_mutex_unlock(&philo->hands.mutex);
	if (philo->action == REST)
		temporary_state(philo, stick);
}

static void	find_stick(t_elem *philo, t_chopstick *stick)
{
	get_free_stick(philo, stick, 2);
	pthread_mutex_lock(&philo->hands.mutex);
	if (philo->hands.stick_nb < 2)
	{
		pthread_mutex_unlock(&philo->hands.mutex);
		get_thinker_stick(philo, stick, 2 - philo->hands.stick_nb);
		pthread_mutex_lock(&philo->hands.mutex);
	}
	set_action(philo);
	pthread_mutex_unlock(&philo->hands.mutex);
}

void		think(t_elem *philo, t_chopstick *stick)
{
	pthread_mutex_lock(&philo->hands.mutex);
	set_action(philo);
	pthread_mutex_unlock(&philo->hands.mutex);
	if (philo->action == REST)
		return ;
	find_stick(philo, stick);
	if (philo->action == THINK)
		post_think_state(philo, stick);
}

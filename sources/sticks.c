/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sticks.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/07 22:46:06 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/27 18:29:13 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>
#include <philo.h>
#include <errno.h>

void		create_sticks(t_clist *philosophers, t_chopstick *stick)
{
	int		i;
	t_elem	*current;

	i = 0;
	current = philosophers->sentinel->next;
	while (i < 7)
	{
		if (current == philosophers->sentinel)
			ft_putendl("Error: sentinel!!");
		stick[i].id = i;
		stick[i].owner = NULL;
		if ((pthread_mutex_init(&stick[i].mutex, NULL)) != 0)
		{
			ft_printf("pthread_mutex_init error: %d\n");
			clst_delete(&philosophers);
			exit(EXIT_FAILURE);
		}
		i++;
		current = current->next;
	}
}

void		get_free_stick(t_elem *philo, t_chopstick *stick, int nb)
{
	int		err;
	int		err2;

	err2 = pthread_mutex_lock(&philo->hands.mutex);
	if ((err = pthread_mutex_trylock(&stick[philo->stick1].mutex)) == 0
			&& stick[philo->stick1].owner == NULL && philo->hands.stick_nb < nb)
	{
		philo->hands.stick_nb += 1;
		stick[philo->stick1].owner = philo;
	}
	if (err == 0)
		pthread_mutex_unlock(&stick[philo->stick1].mutex);
	if ((err = pthread_mutex_trylock(&stick[philo->stick2].mutex)) == 0
			&& stick[philo->stick2].owner == NULL && philo->hands.stick_nb < nb)
	{
		philo->hands.stick_nb += 1;
		stick[philo->stick2].owner = philo;
	}
	if (err == 0)
		pthread_mutex_unlock(&stick[philo->stick2].mutex);
	if (err2 == 0)
		pthread_mutex_unlock(&philo->hands.mutex);
}

static int	stealing_ok(t_elem *philo, t_elem *victim)
{
	int		victim_life;
	int		ret;

	pthread_mutex_lock(&victim->live.mutex);
	victim_life = victim->live.life_nb;
	pthread_mutex_unlock(&victim->live.mutex);
	if (victim_life < philo->live.life_nb)
		ret = 0;
	else
		ret = 1;
	return (ret);
}

static int	steal_stick(t_elem *philo, t_chopstick *stick, int i)
{
	int		ret;
	int		err;
	int		err2;
	t_elem	*victim;

	ret = -1;
	victim = stick[i].owner;
	if (victim == NULL || victim == philo || stealing_ok(philo, victim) == 0)
		return (ret);
	err = pthread_mutex_trylock(&philo->hands.mutex);
	err2 = pthread_mutex_trylock(&victim->hands.mutex);
	if (err == 0 && err2 == 0 && victim->hands.stick_nb == 1)
	{
		stick[i].owner = philo;
		philo->hands.stick_nb += 1;
		victim->hands.stick_nb -= 1;
		ret = 1;
	}
	if (err2 == 0)
		pthread_mutex_unlock(&victim->hands.mutex);
	if (err == 0)
		pthread_mutex_unlock(&philo->hands.mutex);
	return (ret);
}

void		get_thinker_stick(t_elem *philo, t_chopstick *stick, int nb)
{
	t_clist		*philosophers;
	int			ret;
	int			err;

	philosophers = philo_list(NULL);
	err = pthread_mutex_trylock(&stick[philo->stick1].mutex);
	if (err == 0)
	{
		ret = steal_stick(philo, stick, philo->stick1);
		pthread_mutex_unlock(&stick[philo->stick1].mutex);
	}
	err = pthread_mutex_trylock(&stick[philo->stick2].mutex);
	if (nb == 2 || ret == -1)
		steal_stick(philo, stick, philo->stick2);
	if (err == 0)
		pthread_mutex_unlock(&stick[philo->stick2].mutex);
}

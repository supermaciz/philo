/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 20:11:15 by mcizo             #+#    #+#             */
/*   Updated: 2015/03/26 00:00:26 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <philo.h>
#include <SDL_mixer.h>

void	destroy_all_mutex(t_chopstick *stick, t_clist *philosophers)
{
	int		i;
	t_elem	*tmp;

	i = 0;
	tmp = philosophers->sentinel->next;
	while (i < 7)
	{
		pthread_mutex_destroy(&stick[i].mutex);
		if (tmp != philosophers->sentinel)
		{
			pthread_mutex_destroy(&tmp->mutex);
			pthread_mutex_destroy(&tmp->hands.mutex);
			pthread_mutex_destroy(&tmp->live.mutex);
		}
		i++;
	}
}

void	ft_exit(t_data *data, int ex_n, Mix_Chunk *sound)
{
	t_clist		*philosophers;
	t_chopstick	*stick;

	SDL_DestroyWindow(data->win);
	Mix_FreeChunk(sound);
	Mix_CloseAudio();
	SDL_Quit();
	philosophers = philo_list(NULL);
	stick = stick_array(NULL);
	if (stick != NULL)
		destroy_all_mutex(stick, philosophers);
	if (philosophers != NULL)
		clst_delete(&philosophers);
	pthread_mutex_destroy(&data->timer_mutex);
	exit(ex_n);
}

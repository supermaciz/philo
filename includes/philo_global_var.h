/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_global_var.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 16:40:23 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/01 18:00:28 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_GLOBAL_VAR_H
# define PHILO_GLOBAL_VAR_H

# include "philo_structs.h"

extern t_data	*g_data;

#endif

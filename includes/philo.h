/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/06 23:38:57 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/27 17:25:23 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# define MAX_LIFE 25
# define EAT_T 1
# define REST_T 2
# define THINK_T 1
# define TIMEOUT 60

# define LASTPHILO philosophers->sentinel->prev

# include <pthread.h>
# include <time.h>
# include <SDL_mixer.h>
# include "philo_structs.h"

t_clist		*clst_new(void);
void		clstadd_end(t_clist *lst);
void		clstadd_begin(t_clist *lst);
void		clst_del_elem(t_elem *element, t_clist *lst);
void		clst_delete(t_clist **lst);

void		create_sticks(t_clist *philosophers, t_chopstick *sticks);
void		get_free_stick(t_elem *philo, t_chopstick *stick, int nb);
void		get_thinker_stick(t_elem *philo, t_chopstick *stick, int nb);
void		set_action(t_elem *philo);
void		which_action(t_elem *philo, t_chopstick *stick);
t_bool		is_alive(t_elem *philo);
t_clist		*philo_list(t_clist *philosophers);
t_chopstick	*stick_array(t_chopstick *stick);
void		putdown_sticks(t_elem *philo, t_chopstick *stick);
void		*dinner_timer(void *arg);
char		*end_game(char *death);
void		init_threads(void);
void		*philo_actions(void *arg);
char		*start_game(char *go);
void		destroy_all_mutex(t_chopstick *stick, t_clist *philosophers);
void		ft_exit(t_data *data, int ex_n, Mix_Chunk *sound);
void		eat(t_elem *philo, t_chopstick *stick);
void		rest(t_elem *philo, t_chopstick *stick);
void		think(t_elem *philo, t_chopstick *stick);
void		conf_data(t_data *data);
t_elem		*get_next_philo(t_elem *philo);
t_elem		*get_prev_philo(t_elem *philo);
void		post_think_state(t_elem *philo, t_chopstick *stick);

#endif

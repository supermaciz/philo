/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_assets.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 12:28:44 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/01 13:29:58 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDL_ASSETS_H
# define SDL_ASSETS_H

const t_assets	g_assets[] = {
	{793, 359, 17, "ryu_eat.bmp", "ryu_think.bmp", "ryu.wav"},
	{917, 288, 120, "honda_eat.bmp", "honda_think.bmp", "honda.wav"},
	{697, 142, 222, "blanka_eat.bmp", "blanka_think.bmp", "blanka.wav"},
	{479, 308, 323, "guile_eat.bmp", "guile_think.bmp", "guile.wav"},
	{637, 344, 426, "ken_eat.bmp", "ken_think.bmp", "ken.wav"},
	{836, 181, 528, "chunli_eat.bmp", "chunli_think.bmp", "chunli.wav"},
	{552, 178, 629, "dhalsim_eat.bmp", "dhalsim_think.bmp", "dhalsim.wav"}};

#endif

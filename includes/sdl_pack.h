/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_pack.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 00:53:10 by aeddi             #+#    #+#             */
/*   Updated: 2015/04/01 13:42:03 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDL_PACK_H
# define SDL_PACK_H

# define WIDTH_LB		207
# define HEIGHT_LB		16
# define MARGIN			30
# define DIGIT_WIDTH	33
# define DIGIT_Y		16

SDL_Window	*sdl_init(void);
void		sdl_loop(t_data *data);
void		load_background(t_data *data, char *sprite, short update);
void		display_time(SDL_Window *win, int time);
void		play_sound(char *path, short chan, short n_time, Mix_Chunk **sound);
int			update_philo(t_data *data, int philo);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_structs.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/11 20:33:09 by mcizo             #+#    #+#             */
/*   Updated: 2015/04/23 22:46:58 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_STRUCTS_H
# define PHILO_STRUCTS_H

# include <SDL.h>

typedef enum		e_bool
{
	FALSE, TRUE
}					t_bool;

typedef enum		e_action
{
	REST = 0, THINK = 1, EAT = 2
}					t_action;

typedef struct		s_philo_hands
{
	int				stick_nb;
	pthread_mutex_t	mutex;
}					t_philo_hands;

typedef struct		s_lives
{
	int				life_nb;
	time_t			last_meal;
	pthread_mutex_t	mutex;
}					t_lives;

/*
** Each list's element represents a philosopher
*/
typedef struct		s_elem
{
	pthread_t		thread;
	pthread_mutex_t	mutex;
	int				id;
	int				stick1;
	int				stick2;
	t_action		action;
	t_philo_hands	hands;
	t_lives			live;
	struct s_elem	*next;
	struct s_elem	*prev;
}					t_elem;

typedef struct		s_chopstick
{
	int				id;
	t_elem			*owner;
	pthread_mutex_t	mutex;
}					t_chopstick;

typedef struct		s_clist
{
	t_elem			*sentinel;
	size_t			len;
}					t_clist;

typedef struct		s_data
{
	t_elem			*philos[7];
	t_chopstick		*stick;
	t_bool			death;
	size_t			timer;
	pthread_mutex_t	timer_mutex;
	SDL_Window		*win;
}					t_data;

typedef struct		s_assets
{
	int				pos_x;
	int				pos_y;
	int				life_y;
	char			*eat_sp;
	char			*think_sp;
	char			*sound;
}					t_assets;

#endif

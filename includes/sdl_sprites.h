/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sdl_sprites.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aeddi <aeddi@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 12:51:41 by plastic           #+#    #+#             */
/*   Updated: 2015/04/01 13:30:09 by plastic          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SDL_SPRITES_H
# define SDL_SPRITES_H

const char		*g_digit_sprites[] = {
	"./assets/sprites/0.bmp",
	"./assets/sprites/1.bmp",
	"./assets/sprites/2.bmp",
	"./assets/sprites/3.bmp",
	"./assets/sprites/4.bmp",
	"./assets/sprites/5.bmp",
	"./assets/sprites/6.bmp",
	"./assets/sprites/7.bmp",
	"./assets/sprites/8.bmp",
	"./assets/sprites/9.bmp"};

#endif

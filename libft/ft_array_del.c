/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_del.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gponsine <gponsine@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/04 14:27:51 by gponsine          #+#    #+#             */
/*   Updated: 2014/03/07 04:08:48 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_array_del(char ***array_ptr)
{
	char	**array;
	int		i;

	array = (char **)array_ptr;
	i = 0;
	if (array)
	{
		while (array[i])
		{
			ft_putnbr(i);
			ft_putendl("");
			free(array[i++]);
		}
		free(array);
	}
	array = NULL;
}

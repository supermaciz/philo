/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 14:39:50 by mcizo             #+#    #+#             */
/*   Updated: 2015/01/20 18:48:59 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *s1, const void *s2, size_t n)
{
	char	*src;
	char	*dest;
	size_t	i;

	i = 0;
	src = (char *)s2;
	dest = (char *)s1;
	while (src[i] && i < n)
	{
		dest[i] = src[i];
		i++;
	}
	return (s1);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 19:44:06 by mcizo             #+#    #+#             */
/*   Updated: 2013/11/29 18:58:42 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	size_t	i;
	size_t	i2;
	size_t	cpylen;
	size_t	slen;
	char	*copy;

	i = 0;
	i2 = 0;
	cpylen = 0;
	slen = ft_strlen(s);
	if (s == NULL || s[0] == '\0')
		return (NULL);
	while (s[i] != '\0' && (s[i] == ' ' || s[i] == '\n' || s[i] == '\t'))
		i++;
	i2 = i;
	while ((s[slen] == ' ' || s[slen] == '\n' || s[slen] == '\t'
				|| s[slen] == '\0') && slen >= i2)
	{
		i++;
		slen--;
	}
	cpylen = ft_strlen(s) - i + 1;
	copy = ft_strsub(s, (unsigned int)i2, cpylen);
	return (copy);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 22:51:39 by mcizo             #+#    #+#             */
/*   Updated: 2014/04/19 15:00:03 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_atoi2(const char *str, int i)
{
	int	result;

	result = 0;
	i++;
	while (str[i] != '\0' && str[i] >= '0' && str[i] <= '9')
	{
		result *= 10;
		result += (int)str[i] - 48;
		i++;
	}
	return (-1 * (result));
}

int			ft_atoi(const char *str)
{
	int	result;
	int	i;

	result = 0;
	i = 0;
	while (str[i] == ' ' || str[i] == '\r' || str[i] == '\t' || str[i] == '\f'
			|| str[i] == '\v' || str[i] == '\n')
		i++;
	if (str[i] == '-' && ft_isdigit(str[i + 1]) != 0)
	{
		result = ft_atoi2(str, i);
	}
	else if (str[i] == '+')
		i++;
	while (str[i] != '\0' && str[i] >= '0' && str[i] <= '9')
	{
		result *= 10;
		result += (int)str[i] - 48;
		i++;
	}
	return (result);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_append.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/29 03:33:59 by mcizo             #+#    #+#             */
/*   Updated: 2015/01/29 03:45:58 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <libft.h>

char			*ft_append(char *str, char c)
{
	char	*tmp;
	size_t	len;

	if (str == NULL)
	{
		str = ft_strnew(1);
		str[0] = c;
	}
	else
	{
		len = ft_strlen(str);
		tmp = ft_strdup(str);
		ft_strdel(&str);
		str = ft_strnew(len + 1);
		ft_strcpy(str, tmp);
		str[len] = c;
		free(tmp);
	}
	return (str);
}

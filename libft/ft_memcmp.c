/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:41:42 by mcizo             #+#    #+#             */
/*   Updated: 2013/12/04 02:38:16 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t			i;
	unsigned char	*s1bis;
	unsigned char	*s2bis;

	i = 0;
	s1bis = (unsigned char *)s1;
	s2bis = (unsigned char *)s2;
	if ((!s1 && !s2) || (s1bis[0] == '\0' && s2bis[0] == '\0'))
		return (0);
	while (s1bis[i] == s2bis[i] && i < n)
	{
		i++;
	}
	return ((int)(s1bis[i] - s2bis[i]));
}

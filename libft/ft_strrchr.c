/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 18:59:59 by mcizo             #+#    #+#             */
/*   Updated: 2014/01/12 21:57:38 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int		i;
	char	*ret;

	ret = (char *)s;
	ret += ft_strlen(s);
	i = ft_strlen(s);
	while (ret && s[i] != c)
	{
		ret--;
		i--;
	}
	if (i < 0)
	{
		return (NULL);
	}
	else
	{
		return (ret);
	}
}

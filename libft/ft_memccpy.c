/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:31:18 by mcizo             #+#    #+#             */
/*   Updated: 2013/11/25 05:30:39 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	size_t	i;
	char	*src;
	char	*dest;

	i = 0;
	src = (char *)s2;
	dest = (char *)s1;
	while (src[i] != (unsigned char)c && i < n)
	{
		dest[i] = src[i];
		i++;
	}
	if (src[i] == (unsigned char)c)
	{
		dest[i] = src[i];
		i++;
		return (dest + i);
	}
	else
	{
		return (NULL);
	}
}

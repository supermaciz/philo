/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 09:20:31 by mcizo             #+#    #+#             */
/*   Updated: 2015/02/05 05:25:45 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

/*
** Use a negative fd if you don't want to save it.
*/

static void		which_function_fd(t_param *printf_arg, int *len, va_list ap)
{
	void	(*flaglist[122])(t_param *, int *);

	flaglist[37] = print_char_fd;
	flaglist[99] = print_char_fd;
	flaglist[100] = print_integer_fd;
	flaglist[105] = print_integer_fd;
	flaglist[115] = print_str_fd;
	flaglist[111] = print_oct_int_fd;
	flaglist[117] = print_u_int_fd;
	if (printf_arg->type == 37)
		printf_arg->value.c = '%';
	else
		printf_arg->value = va_arg(ap, t_content);
	flaglist[(int)printf_arg->type](printf_arg, len);
}

int				get_printf_fd(int fd)
{
	static int		saved_fd = -42;

	if (fd > -1)
	{
		saved_fd = fd;
	}
	return (saved_fd);
}

int				ft_printf_fd(int fd, const char *format, ...)
{
	int			index;
	int			len;
	va_list		ap;
	t_param		printf_arg;

	va_start(ap, format);
	index = 0;
	len = 0;
	get_printf_fd(fd);
	while (format[index] != '\0')
	{
		while (format[index] && format[index] != '%')
			print_format_fd(format, &index, &len);
		if (format[index] == '%'
			&& (ft_isalpha(format[index + 1]) == 1 || format[index + 1] == '%'))
		{
			printf_arg.type = format[index + 1];
			which_function_fd(&printf_arg, &len, ap);
			index += 2;
		}
	}
	va_end(ap);
	return (len);
}

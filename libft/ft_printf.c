/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/12 09:20:31 by mcizo             #+#    #+#             */
/*   Updated: 2014/03/27 01:19:05 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void		which_function(t_param *printf_arg, int *len, va_list ap)
{
	void	(*flaglist[122])(t_param *, int *);

	flaglist[37] = print_char;
	flaglist[99] = print_char;
	flaglist[100] = print_integer;
	flaglist[105] = print_integer;
	flaglist[115] = print_str;
	flaglist[111] = print_oct_int;
	flaglist[117] = print_u_int;
	if (printf_arg->type == 37)
		printf_arg->value.c = '%';
	else
		printf_arg->value = va_arg(ap, t_content);
	flaglist[(int)printf_arg->type](printf_arg, len);
}

int				ft_printf(const char *format, ...)
{
	int			index;
	int			len;
	va_list		ap;
	t_param		printf_arg;

	va_start(ap, format);
	index = 0;
	len = 0;
	while (format[index] != '\0')
	{
		while (format[index] && format[index] != '%')
			print_format(format, &index, &len);
		if (format[index] == '%'
			&& (ft_isalpha(format[index + 1]) == 1 || format[index + 1] == '%'))
		{
			printf_arg.type = format[index + 1];
			which_function(&printf_arg, &len, ap);
			index += 2;
		}
	}
	va_end(ap);
	return (len);
}

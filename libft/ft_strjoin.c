/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 18:57:17 by mcizo             #+#    #+#             */
/*   Updated: 2013/11/28 19:37:52 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*newstr;

	newstr = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + 1);
	ft_memcpy(newstr, s1, ft_strlen(s1));
	ft_strcat(newstr, s2);
	return (newstr);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 04:39:36 by mcizo             #+#    #+#             */
/*   Updated: 2015/02/18 23:34:07 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*strrevcpy(char *str)
{
	int		i;
	size_t	len;
	char	*revcpy;

	i = 0;
	len = ft_strlen(str);
	revcpy = ft_strnew(len);
	while (len > 0)
	{
		revcpy[i] = str[len - 1];
		len--;
		i++;
	}
	revcpy[i] = '\0';
	ft_strdel(&str);
	return (revcpy);
}

static int	digit_counter(int n)
{
	int		len;

	len = 0;
	while (n)
	{
		n /= 10;
		len++;
	}
	return (len);
}

static char	*ft_itoa_2(int n)
{
	char	*tmp;
	int		i;
	int		len;

	i = 0;
	if (n == -2147483648)
	{
		tmp = ft_strnew(12);
		return (tmp = ft_strcpy(tmp, "-2147483648"));
	}
	n = n * -1;
	len = digit_counter(n);
	tmp = ft_strnew(len + 1);
	while (n)
	{
		tmp[i] = (n % 10) + 48;
		n = n / 10;
		i++;
	}
	tmp[i] = '-';
	return (strrevcpy(tmp));
}

static char	*return_zero(void)
{
	char	*ret;

	ret = ft_strnew(1);
	ret[0] = '0';
	return (ret);
}

char		*ft_itoa(int n)
{
	int		result;
	int		i;
	char	*tmp;
	int		len;

	i = 0;
	result = n;
	if (n > 0)
	{
		len = digit_counter(n);
		tmp = ft_strnew(len);
		while (result)
		{
			tmp[i] = (result % 10) + 48;
			result = result / 10;
			i++;
		}
		return (strrevcpy(tmp));
	}
	else if (n < 0)
		return (ft_itoa_2(n));
	else if (n == 0)
		return (return_zero());
	else
		return (NULL);
}

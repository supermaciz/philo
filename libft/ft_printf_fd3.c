/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fd3.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 00:20:20 by mcizo             #+#    #+#             */
/*   Updated: 2015/02/05 05:25:47 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static void		ft_free_oct(char *result, char *tmp2)
{
	free(result);
	free(tmp2);
}

void			print_oct_int_fd(t_param *printf_param, int *len)
{
	char			*result;
	char			*tmp;
	char			*tmp2;
	unsigned int	mod;

	result = ft_strnew(0);
	while (printf_param->value.u >= 8)
	{
		mod = printf_param->value.u % 8;
		printf_param->value.u = (printf_param->value.u - mod) / 8;
		tmp2 = ft_itoa(mod);
		tmp = ft_strjoin(tmp2, result);
		ft_free_oct(result, tmp2);
		result = ft_strdup(tmp);
		free(tmp);
	}
	tmp2 = ft_itoa(printf_param->value.u);
	tmp = ft_strjoin(tmp2, result);
	ft_free_oct(result, tmp2);
	result = ft_strdup(tmp);
	free(tmp);
	ft_putstr_fd(result, get_printf_fd(-1));
	*len += ft_strlen(result);
	free(result);
}

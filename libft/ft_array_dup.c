/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_dup.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gponsine <gponsine@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/03 11:40:45 by gponsine          #+#    #+#             */
/*   Updated: 2015/01/20 20:52:32 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	**ft_array_dup(void *array_ptr)
{
	char			**array;
	char			**dup;
	size_t			i;

	array = (char **)array_ptr;
	i = 0;
	while (array[i])
		i++;
	dup = (char **)ft_memalloc(sizeof(char *) * (i + 1));
	i = 0;
	while (array[i])
	{
		dup[i] = ft_strdup(array[i]);
		i++;
	}
	dup[i] = 0;
	return (dup);
}

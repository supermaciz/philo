# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/14 16:00:35 by aeddi             #+#    #+#              #
#    Updated: 2015/04/27 00:17:33 by mcizo            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME			=	philo
CC				=	clang
GDB				?=	0
CFLAGS			=	-Wall -Wextra -Werror -pedantic -I $(LIBFT_DIR) -I $(INCS_DIR) -I $(LIBFT_INC_DIR)
CFLAGS 			+=	`SDL2/bin/sdl2-config --cflags`
CFLAGS	 		+= 	-g3
LFLAGS			=	-L $(LIBFT_DIR) -lft `SDL2/bin/sdl2-config --libs` -lSDL2_mixer
LIBFT_DIR		=	./libft
LIBFT_INC_DIR	= 	./libft/includes
INCS_DIR		=	./includes
OBJS_DIR		=	./objects
SRCS_DIR		=	./sources
OBJS			=	$(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))
SRCS			=	sdl_functions.c			\
					sdl_render.c			\
					sdl_audio.c				\
					sdl_time.c				\
					circ_list.c				\
					init_threads.c			\
					main.c					\
					philo_actions.c			\
					philo_thread.c			\
					putdown_sticks.c		\
					sticks.c				\
					time.c					\
					exit.c					\
					which_action.c			\
					conf_data.c				\
					next_prev_philo.c		\
					philolist_sticks.c		\
					post_think.c

all				:	SDL2 $(NAME)

$(NAME)			:	$(OBJS_DIR) $(OBJS)
	@echo "Linking $@"
	$(CC) -o $@ $(OBJS) $(LFLAGS)

$(OBJS_DIR)/%.o	:	$(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^

$(OBJS_DIR)		:	make_libft
	@mkdir -p $(OBJS_DIR)

make_libft		:
	@$(MAKE) -C $(LIBFT_DIR)

SDL2:
	@mkdir -p SDL2/install
	@if [[ ! -f SDL2/install/SDL2-2.0.3/Makefile ]] ;				\
	then 															\
		tar xzf .SDL2-2.0.3.tar.gz -C SDL2/install					\
		&& tar xzf .SDL2_mixer-2.0.0.tar.gz -C SDL2/install			\
		&& ( cd SDL2/install/SDL2-2.0.3								\
			&& ./configure CC=clang --prefix=$(shell pwd)/SDL2/		\
			&& $(MAKE) && $(MAKE) install )							\
		&& ( cd SDL2/install/SDL2_mixer-2.0.0						\
			&& ./configure CC=clang									\
			CFLAGS='-g -O2 -I./VisualC/external/include'			\
			--prefix=$(shell pwd)/SDL2/								\
			--exec-prefix=$(shell pwd)/SDL2/						\
			&& $(MAKE) && $(MAKE) install ) ;						\
	fi ;

fclean			:	clean
	@echo "Removing $(NAME)"
	rm -f $(NAME)
	rm -rf SDL2

clean			:
	@echo "Removing $(OBJS_DIR)"
	rm -rf $(OBJS_DIR)
	rm -rf SDL2/install

re				:	fclean all

fclean2			:	clean2
	@echo "Removing $(NAME)"
	rm -f $(NAME)

clean2			:
	@echo "Removing $(OBJS_DIR)"
	rm -rf $(OBJS_DIR)

re2				:	fclean2 all

.PHONY: clean fclean re all SDL2 fclean2 clean2 re2
